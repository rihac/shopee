package controllers

import (
	"encoding/json"
	"errors"
	// "errors"
	// "fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"

	"github.com/riyan/API-shopee/models"
	"github.com/riyan/mysql_dba"
	"github.com/riyan/webserver/webhandler"
)

func (wh eHandler) handleGETMe(w http.ResponseWriter, r *http.Request) webhandler.Response {
	res := webhandler.Response{}
	paths := UrlPath(r.URL, wh.pattern)

	db, err := webhandler.DBFromContext(r.Context())
	if err != nil {
		res.Error(err, http.StatusInternalServerError)
		return res
	}
	if len(paths) == 2 {
		mgrvcode, err := url.PathUnescape(paths[1])
		// fmt.Printf("mgrvcode : %s", mgrvcode)
		if err != nil {
			res.Error(err, http.StatusInternalServerError)
			return res
		}

		if mgrvcode != "" {
			id, _ := strconv.Atoi(mgrvcode)
			Em := &models.Me{MeCode: id}
			err := Em.Get(db)
			if err != nil {
				res.Error(err, http.StatusOK)
				return res
			}
			res.Data = Em
			return res
		}

	}
	var values = r.URL.Query()
	set, cursor, err := QueryCursor(values)
	if err != nil {
		res.Error(err, http.StatusBadRequest)
	}
	if !set {
		flds, err := QueryFields(values)
		if err != nil {
			res.Error(err, http.StatusBadRequest)
			return res
		}
		lmt, err := QueryLimit(values)
		if err != nil {
			res.Error(err, http.StatusBadRequest)
			return res
		}
		srt, asc, err := QuerySort(values)
		if err != nil {
			res.Error(err, http.StatusBadRequest)
			return res
		}
		fil, err := QueryFilter(values)
		if err != nil {
			res.Error(err, http.StatusBadRequest)
			return res
		}
		cursor = mysql_dba.Cursor{
			Fields:     flds,
			Filters:    fil,
			OrderBy:    srt,
			Descending: asc,
			Limit:      lmt,
		}
	}

	var mes []mysql_dba.Table
	tem := &models.Me{}
	mes, cursor, err = mysql_dba.Fetch(db, tem, cursor)
	if err != nil {
		res.Error(err, http.StatusOK)
		return res
	}
	res.Data = mes
	res.Cursor = cursor
	return res
}
func (wh eHandler) handlePOSTMe(w http.ResponseWriter, r *http.Request) webhandler.Response {
	res := webhandler.Response{}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		res.Error(err, http.StatusBadRequest)
		return res
	}
	em := &models.Me{}
	if err := json.Unmarshal(body, em); err != nil {
		res.Error(err, http.StatusBadRequest)
		return res
	}
	db, err := webhandler.DBFromContext(r.Context())
	if err != nil {
		res.Error(err, http.StatusInternalServerError)
		return res
	}
	tx, err := db.Begin()
	if err != nil {
		res.Error(err, http.StatusInternalServerError)
		return res
	}
	if err = em.Insert(tx); err != nil {
		res.Error(err, http.StatusOK)
		tx.Rollback()
		return res
	}
	if err = tx.Commit(); err != nil {
		res.Error(err, http.StatusInternalServerError)
		return res
	}
	res.Data = em
	return res
}
func (wh eHandler) handleDELETEEmployed(w http.ResponseWriter, r *http.Request) webhandler.Response {
	res := webhandler.Response{}
	paths := UrlPath(r.URL, wh.pattern)
	// fmt.Printf("len path:%d last:%s\n", len(paths), paths[len(paths)-1])
	last, err := url.PathUnescape(paths[1])
	// fmt.Printf("id : %s", id)
	if err != nil {
		res.Error(err, http.StatusInternalServerError)
		return res
	}
	if last == "" || last == "me" {
		res.Error(errors.New("specify id to delete"), http.StatusOK)
		return res
	}
	id, _ := strconv.Atoi(last)
	db, err := webhandler.DBFromContext(r.Context())
	if err != nil {
		res.Error(err, http.StatusInternalServerError)
		return res
	}
	tx, err := db.Begin()
	if err != nil {
		res.Error(err, http.StatusInternalServerError)
		return res
	}
	data := make(map[string]interface{})
	data["IsActive"] = false
	em := models.Me{MeCode: id}
	if _, err = em.Update(tx, data); err != nil {
		res.Error(err, http.StatusOK)
		tx.Rollback()
		return res
	}
	if err = tx.Commit(); err != nil {
		res.Error(err, http.StatusInternalServerError)
		return res
	}
	return res
}
