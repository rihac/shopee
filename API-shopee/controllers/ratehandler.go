
package controllers

import (
	"encoding/json"
	// "time"
	// "errors"
	// "fmt"
	"io/ioutil"
	"net/http"
	// "net/url"
	// "strconv"
	"github.com/riyan/API-shopee/models"
	"github.com/riyan/mysql_dba"
	"github.com/riyan/webserver/webhandler"
)
type DatePar struct{
	RateDate string `json:"ratedate"`
}
func (wh eHandler) handleGETRate(w http.ResponseWriter, r *http.Request) webhandler.Response {
	res := webhandler.Response{}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		res.Error(err, http.StatusOK)
		return res
	}
	db, err := webhandler.DBFromContext(r.Context())
	if err != nil {
		res.Error(err, http.StatusInternalServerError)
		return res
	}
	var param []DatePar
	if err := json.Unmarshal(body, &param); err != nil {
		res.Error(err, http.StatusOK)
		return res
	}
	var pr []interface{}
	for _,v := range param{
		pr = append(pr,v)
	} 
	var mes []mysql_dba.StoreProcedure
	tem := &models.SpRate{}
	mes, err = mysql_dba.FetchSP(db, tem, pr)
	if err != nil {
		res.Error(err, http.StatusOK)
		return res
	}
	res.Data = mes
	return res
}