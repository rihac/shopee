package models

import (
	"time"

	"github.com/riyan/mysql_dba"
)
var MeTable = `CREATE TABLE MasterExchange
(
	me_code int PRIMARY KEY AUTO_INCREMENT,
    me_from varchar(100) not null,
	me_to varchar(100) not null,
	is_Active bool not null,
	CreationTime datetime not null,
	DeletionTime Datetime 
	);`

type Me struct{
	MeCode 			int			`json:"me_code"`
	MeFrom 			string		`json:"me_from"`
	MeTo			string		`json:"me_to"`
	IsActive		bool		`json:"is_Active"`
	CreationTime	time.Time	`json:"CreationTime"`
	DeletionTime	time.Time	`json:"DeletionTime"`
}

func Mes(db mysql_dba.DBExecer, c mysql_dba.Cursor) ([]*Me, mysql_dba.Cursor, error) {
	e := &Me{}
	res, cur, err := mysql_dba.Fetch(db, e, c)
	if err != nil {
		return nil, c, err
	}
	mes := make([]*Me, len(res))
	for i, v := range res {
		mes[i] = v.(*Me)
	}
	return mes, cur, nil
}

func (m *Me) Name() string{
	return "MasterExchange"
}
func (m *Me) New() mysql_dba.Table {
	return &Me{}
}
func (m *Me) PrimaryKey() (fields []string, dst []interface{}){
	fields = []string {"me_code"}
	dst = []interface{}{&m.MeCode}
	return fields, dst
}

func (m *Me) Fields() (fields []string, dst []interface{}){
	fields = []string{"me_code","me_from","me_to","is_Active","CreationTime","DeletionTime"}
	dst = []interface{}{&m.MeCode, &m.MeFrom, &m.MeTo, &m.IsActive, &m.CreationTime, &m.DeletionTime}
	return fields, dst
}

func (m *Me) HasAutoIncrementField() bool{
	return true
}

func (m *Me) Insert(db mysql_dba.DBExecer) error {
	m.CreationTime = time.Now()
	m.IsActive = true
	return mysql_dba.Insert(db, m)
}

// Update fungsi mengedit data eset
func (m *Me) Update(db mysql_dba.DBExecer, change map[string]interface{}) (map[string]interface{}, error) {
	err := mysql_dba.Update(db, m, change)
	return change, err
}

//Delete fungsi mengahapus data eset
func (m *Me) Delete(db mysql_dba.DBExecer) error {
	return mysql_dba.Delete(db, m)
}

//Get fungsi untuk mengambil data eset
func (m *Me) Get(db mysql_dba.DBExecer) error {
	return mysql_dba.Get(db, m)
}