package models 

import (
	"time"
	"github.com/riyan/mysql_dba"
)

var TrETable =`CREATE TABLE TR_Exchange
(
	tre_code int PRIMARY KEY AUTO_INCREMENT,
	me_code int not null,
	tr_date datetime not null,
	rate double not null,
	is_Active bool not null,
	CreationTime Datetime not null ,
	DeletionTime Datetime 
	);
`

type Tre struct{
	TreCode			int			`json:"tre_code"`
	MeCode 			int			`json:"me_code"`
	TreDate			time.Time	`json:"tre_date"`
	Rate			float64		`json:"rate"`
	IsActive		bool		`json:"is_Active"`
	CreationTime	time.Time	`json:"CreationTime"`
	DeletionTime	time.Time	`json:"DeletionTime"`
}

func Tres(db mysql_dba.DBExecer, c mysql_dba.Cursor) ([]*Tre, mysql_dba.Cursor, error) {
	e := &Tre{}
	res, cur, err := mysql_dba.Fetch(db, e, c)
	if err != nil {
		return nil, c, err
	}
	tres := make([]*Tre, len(res))
	for i, v := range res {
		tres[i] = v.(*Tre)
	}
	return tres, cur, nil
}

func (m *Tre) Name() string{
	return "TR_Exchange"
}
func (m *Tre) New() mysql_dba.Table {
	return &Tre{}
}

func (m *Tre) PrimaryKey() (fields []string, dst []interface{}){
	fields = []string {"tre_code"}
	dst = []interface{}{&m.TreCode}
	return fields, dst
}

func (m *Tre) Fields() (fields []string, dst []interface{}){
	fields = []string{"tre_code","me_code","tre_date","rate","is_Active","CreationTime","DeletionTime"}
	dst = []interface{}{&m.TreCode, &m.MeCode, &m.TreDate, &m.Rate, &m.IsActive, &m.CreationTime, &m.DeletionTime}
	return fields, dst
}

func (m *Tre) HasAutoIncrementField() bool{
	return true
}

func (m *Tre) Insert(db mysql_dba.DBExecer) error {
	return mysql_dba.Insert(db, m)
}

// Update fungsi mengedit data eset
func (m *Tre) Update(db mysql_dba.DBExecer, change map[string]interface{}) (map[string]interface{}, error) {
	err := mysql_dba.Update(db, m, change)
	return change, err
}

//Delete fungsi mengahapus data eset
func (m *Tre) Delete(db mysql_dba.DBExecer) error {
	return mysql_dba.Delete(db, m)
}

//Get fungsi untuk mengambil data eset
func (m *Tre) Get(db mysql_dba.DBExecer) error {
	return mysql_dba.Get(db, m)
}

