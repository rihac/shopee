package models
import (
	"time"

	"github.com/riyan/mysql_dba"
)

var Sp_rate = `
USE 'test_dbaccess';
DROP procedure IF EXISTS 'get_excange_rate';

DELIMITER $$
USE 'test_dbaccess'$$
CREATE PROCEDURE 'get_excange_rate' (tr_datex date)
BEGIN
	select * from tr_exchange a left join
    masterexchange  b on a.me_code = b.me_code where 
    date(tr_date) between tr_datex and adddate(tr_datex,-7); 
END$$

DELIMITER ;
`

type SpRate struct {
	MeFrom 			string		`json:"me_from"`
	MeTo			string		`json:"me_to"`
	TreDate			time.Time	`json:"tre_date"`
	Rate			float64		`json:"rate"`
}

func SpRates(db mysql_dba.DBExecer,z []interface{}) ([]*SpRate, error) {
	e := &SpRate{}
	res, err := mysql_dba.FetchSP(db, e, z )
	if err != nil {
		return nil, err
	}
	mes := make([]*SpRate, len(res))
	for i, v := range res {
		mes[i] = v.(*SpRate)
	}
	return mes, nil
}

func (m *SpRate) Name() string{
	return "MasterExchange"
}
func (m *SpRate) New() mysql_dba.StoreProcedure {
	return &SpRate{}
}

func (m *SpRate) Fields() (fields []string, dst []interface{}){
	fields = []string{"me_from","me_to","tre_date","rate"}
	dst = []interface{}{&m.MeFrom, &m.MeTo, &m.TreDate, &m.Rate}
	return fields, dst
}
